package base;

public class Main {

	public static void main(String[] a) {

		Movie m1 = new Movie("M1", 0);

		Movie m2 = new Movie("M2", 1);

		Movie m3 = new Movie("M3", 1);

		Movie m4 = new Movie("M4", 2);

		Movie m5 = new Movie("M5", 2);

		Customer c1 = new Customer("Moi");

		Rental r1 = new Rental(m5, 5);

		c1.addRentals(r1);

		c1.addRentals(new Rental(m1, 10));

		c1.addRentals(new Rental(m3, 5));

		System.out.println(c1.statement());

	}

}
