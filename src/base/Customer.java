package base;

import java.util.*;

public class Customer {

	private java.lang.String name_;

	private Vector<Rental> rentals_;

	public Customer(String name) {

		name_ = name;

		rentals_ = new Vector<Rental>();

	}

	public void addRentals(Rental arg) {

		rentals_.addElement(arg);

	}

	public java.lang.String getName() {

		return name_;

	}

	public String statement() {

		double totalAmount = 0;

		int frequentRenterPoints = 0;

		Enumeration<Rental> rentals = rentals_.elements();

		String result = "Rental Record for " + getName() + "\n";

		while (rentals.hasMoreElements()) {

			Rental each = (Rental) rentals.nextElement();

			double thisAmount = each.amoutFor();

			// add frequent renter points

			frequentRenterPoints++;

			// add bonus for a two day new release rental

			if (each.getMovie().getPriceCode() == Movie.NEW_RELEASE) {

				frequentRenterPoints++;

			}

			// show figures for this rental

			result += "\t" + each.getMovie().getTitle()

					+ "\t" + String.valueOf(thisAmount) + "\n";

			totalAmount += thisAmount;

		}

// add footer lines

		result += "Amount owed is "

				+ String.valueOf(totalAmount)

				+ "\n";

		result += "You earned "

				+ String.valueOf(frequentRenterPoints)

				+ "frequent renter points ";

		return result;

	}

}
